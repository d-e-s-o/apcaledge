[![pipeline](https://gitlab.com/d-e-s-o/apcaledge/badges/master/pipeline.svg)](https://gitlab.com/d-e-s-o/apcaledge/-/commits/master)
[![crates.io](https://img.shields.io/crates/v/apcaledge.svg)](https://crates.io/crates/apcaledge)
[![rustc](https://img.shields.io/badge/rustc-1.56+-blue.svg)](https://blog.rust-lang.org/2021/10/21/Rust-1.56.0.html)

apcaledge
=========

- [Changelog](CHANGELOG.md)

**apcaledge** is an application for exporting trading activity from
Alpaca at [alpaca.markets][] in a [Ledger CLI][ledger-cli] compatible
format.
The program is powered by the [`apca`][apca] crate and written in Rust.


[alpaca.markets]: https://alpaca.markets
[apca]: https://crates.io/crates/apca
[ledger-cli]: https://www.ledger-cli.org/
